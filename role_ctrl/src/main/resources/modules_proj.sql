/*
Navicat MySQL Data Transfer

Source Server         : Jasonwe
Source Server Version : 80020
Source Host           : localhost:3306
Source Database       : modules_proj

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2021-01-26 23:34:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for m_menu
-- ----------------------------
DROP TABLE IF EXISTS `m_menu`;
CREATE TABLE `m_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜单名',
  `path` varchar(64) DEFAULT NULL,
  `name_zh` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `component` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of m_menu
-- ----------------------------
INSERT INTO `m_menu` VALUES ('1', 'admin', '/admin', '管理页面', null, null);
INSERT INTO `m_menu` VALUES ('2', 'personal', '/personal', '个人主页', null, null);

-- ----------------------------
-- Table structure for m_perm
-- ----------------------------
DROP TABLE IF EXISTS `m_perm`;
CREATE TABLE `m_perm` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of m_perm
-- ----------------------------
INSERT INTO `m_perm` VALUES ('1', 'user_manage', '普通用户路径', '/module/user');
INSERT INTO `m_perm` VALUES ('2', 'admin_manage', '管理员权限路径', '/module/admin');

-- ----------------------------
-- Table structure for m_role
-- ----------------------------
DROP TABLE IF EXISTS `m_role`;
CREATE TABLE `m_role` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '角色名称',
  `name_zh` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of m_role
-- ----------------------------
INSERT INTO `m_role` VALUES ('4', 'admin', '管理员');
INSERT INTO `m_role` VALUES ('5', 'employee', '普通员工');

-- ----------------------------
-- Table structure for m_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `m_role_menu`;
CREATE TABLE `m_role_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `rid` int DEFAULT NULL COMMENT '角色id',
  `mid` int DEFAULT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of m_role_menu
-- ----------------------------
INSERT INTO `m_role_menu` VALUES ('2', '4', '1');
INSERT INTO `m_role_menu` VALUES ('3', '4', '2');
INSERT INTO `m_role_menu` VALUES ('4', '5', '2');

-- ----------------------------
-- Table structure for m_role_perm
-- ----------------------------
DROP TABLE IF EXISTS `m_role_perm`;
CREATE TABLE `m_role_perm` (
  `id` int NOT NULL AUTO_INCREMENT,
  `rid` int DEFAULT NULL,
  `pid` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of m_role_perm
-- ----------------------------
INSERT INTO `m_role_perm` VALUES ('1', '4', '1');
INSERT INTO `m_role_perm` VALUES ('2', '4', '2');
INSERT INTO `m_role_perm` VALUES ('3', '5', '1');

-- ----------------------------
-- Table structure for m_user
-- ----------------------------
DROP TABLE IF EXISTS `m_user`;
CREATE TABLE `m_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int DEFAULT '3' COMMENT '角色id',
  `nickname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL COMMENT '密码的盐',
  `phone` varchar(32) DEFAULT NULL,
  `mail` varchar(64) DEFAULT NULL,
  `sex` enum('男','女','保密') DEFAULT '保密',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of m_user
-- ----------------------------
INSERT INTO `m_user` VALUES ('21', '5', 'Tom', '3c764adc28ddc5d06e814f5081c381a2', 'HcIV8VXEMAe/DGxTOJmJGw==', null, '15071488216@163.com', '保密');
