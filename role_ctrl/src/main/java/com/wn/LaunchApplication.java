package com.wn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: projwe9
 * @description: 项目启动类
 * @author: Jason-Wei
 * @create: 2021-01-15 16:21
 **/
@SpringBootApplication
@MapperScan("com.wn.*.mapper")
public class LaunchApplication {

    public static void main(String[] args) {
        SpringApplication.run(LaunchApplication.class, args);
    }

}
