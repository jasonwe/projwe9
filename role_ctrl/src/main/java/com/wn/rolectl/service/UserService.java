package com.wn.rolectl.service;

import com.wn.rolectl.entity.Role;
import com.wn.rolectl.entity.User;
import com.wn.rolectl.mapper.RoleMapper;
import com.wn.rolectl.mapper.UserMapper;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author Jason-we
 * @program: modules
 * @Description: 用户服务类
 * @date 2020-12-22-10-59
 **/
@Service
public class UserService implements UserMapper {

    @Autowired
    UserMapper userMapper;

    @Autowired
    RoleMapper roleMapper;

    @Override
    public List<User> getAllUser() {
        return userMapper.getAllUser();
    }

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public User getUserByPhone(String phone) {
        return userMapper.getUserByPhone(phone);
    }

    @Override
    public User getUserByMail(String mail) {
        return userMapper.getUserByMail(mail);
    }

    @Override
    public String getPassByPhone(String phone) {
        return userMapper.getPassByPhone(phone);
    }

    @Override
    public String getPassByMail(String mail) {
        return userMapper.getPassByMail(mail);
    }

    @Override
    public int updatePassByPhone(String pass, String phone) {
        return userMapper.updatePassByPhone(pass,phone);
    }

    @Override
    public int updatePassByMail(String pass, String mail) {
        return userMapper.updatePassByMail(pass,mail);
    }

    @Override
    public int updatePassByMail1(User user) {
        return userMapper.updatePassByMail1(user);
    }

    @Override
    public int updatePassByPhone1(User user) {
        return userMapper.updatePassByPhone1(user);
    }

    public User findByUser(User user){
        if(Objects.isNull(user.getPhone())){
            return getUserByMail(user.getMail());
        }else{
            return getUserByPhone(user.getPhone());
        }
    }

    public User findByUserType(String type){
        if(type.indexOf('@') != -1){
            return getUserByMail(type);
        }else{
            return getUserByPhone(type);
        }
    }

    public int userRegis(User user){
        // 默认生成 16 位盐
        String salt = new SecureRandomNumberGenerator().nextBytes().toString();
        int times = 2;
        String encodedPassword = new SimpleHash("md5", user.getPassword(), salt, times).toString();

        user.setSalt(salt);
        user.setPassword(encodedPassword);

        return addUser(user);

    }

    public int userPassReset(User user){
        String salt = new SecureRandomNumberGenerator().nextBytes().toString();
        int times = 2;
        user.setSalt(salt);
        String encodedPassword = new SimpleHash("md5", user.getPassword(), salt, times).toString();
        user.setPassword(encodedPassword);

        if(Objects.isNull(user.getPhone())){
            return updatePassByMail1(user);
        }else{
            return updatePassByPhone1(user);
        }
    }


    public String getRole(String user){
        User u = findByUserType(user);
        return roleMapper.getRoleName(u.getRole_id());
    }
}
