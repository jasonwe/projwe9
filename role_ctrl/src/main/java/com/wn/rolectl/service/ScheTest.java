package com.wn.rolectl.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

/**
 * @program: projwe9
 * @description: 测试shce
 * @author: Jason-Wei
 * @create: 2021-01-13 15:59
 **/
@Component
@Slf4j
public class ScheTest {

    @Bean
    public TaskScheduler taskScheduler(){
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(3);
        return scheduler;
    }


    @Scheduled(cron = "0 23 16 * * *")
    public void taskA() throws InterruptedException {
        log.info("taskA running");
        Thread.sleep(60 * 1000);
    }


    @Scheduled(cron = "0 24 16 * * *")
    public void taskB() {
        log.info("taskB running");
    }

    @Scheduled(initialDelay=30000, fixedDelay=60000)
    public void taskC() throws InterruptedException {
        log.info("taskC running");
        Thread.sleep(2* 60 * 1000);
    }
}
