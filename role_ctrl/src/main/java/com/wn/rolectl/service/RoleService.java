package com.wn.rolectl.service;

import com.wn.rolectl.entity.Role;
import com.wn.rolectl.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: projwe9
 * @description: role
 * @author: Jason-Wei
 * @create: 2021-01-27 11:52
 **/
@Service
public class RoleService implements RoleMapper {

    @Autowired
    RoleMapper roleMapper;

    @Override
    public List<Role> getAllRole() {
        return roleMapper.getAllRole();
    }

    @Override
    public String getRoleName(int id) {
        return roleMapper.getRoleName(id);
    }
}
