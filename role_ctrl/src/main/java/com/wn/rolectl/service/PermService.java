package com.wn.rolectl.service;

import com.wn.rolectl.entity.Perm;
import com.wn.rolectl.mapper.PermMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @program: projwe9
 * @description: 路径权限
 * @author: Jason-Wei
 * @create: 2021-01-27 15:13
 **/
@Service
public class PermService implements PermMapper {

    @Autowired
    PermMapper permMapper;

    @Override
    public List<String> getAllUrl() {
        return permMapper.getAllUrl();
    }

    @Override
    public Set<String> getAllUrlById(List<Integer> pids) {
        return permMapper.getAllUrlById(pids);
    }

    public boolean needFilter(String reqApi){
        for(String url : getAllUrl()){
            if(reqApi.startsWith(url)){
                return true;
            }
        }
        return false;
    }

}
