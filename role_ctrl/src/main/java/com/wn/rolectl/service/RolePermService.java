package com.wn.rolectl.service;

import com.wn.rolectl.mapper.RolePermMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: projwe9
 * @description: role perm
 * @author: Jason-Wei
 * @create: 2021-01-27 11:54
 **/
@Service
public class RolePermService implements RolePermMapper {

    @Autowired
    RolePermMapper rolePermMapper;


    @Override
    public List<Integer> getAllPidByRid(int rid) {
        return rolePermMapper.getAllPidByRid(rid);
    }
}
