package com.wn.rolectl.mapper;

import com.wn.rolectl.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author Jason-we
 * @program: modules
 * @Description: 用户mapper映射
 * @date 2020-12-22-10-55
 **/
public interface UserMapper {

    @Select("select * from m_user")
    List<User> getAllUser();

    @Insert("insert into m_user (nickname,phone,mail,password,salt) values(#{nickname},#{phone},#{mail},#{password},#{salt})")
    int addUser(User user);

    @Select("select * from m_user where phone= #{phone}")
    User getUserByPhone(@Param("phone") String phone);

    @Select("select * from m_user where mail= #{mail}")
    User getUserByMail(@Param("mail") String mail);

    @Select("select password from m_user where phone = #{phone}")
    String getPassByPhone(@Param("phone") String phone);

    @Select("select password from m_user where mail = #{mail}")
    String getPassByMail(@Param("mail") String mail);

    @Update("update m_user set password = #{pass} where phone = #{phone}")
    int updatePassByPhone(@Param("pass") String pass,@Param("phone") String phone);

    @Update("update m_user set password = #{pass} where mail = #{mail}")
    int updatePassByMail(@Param("pass") String pass,@Param("mail") String mail);

    @Update("update m_user set password = #{password},salt = #{salt} where mail = #{mail}")
    int updatePassByMail1(User user);

    @Update("update m_user set password = #{password},salt = #{salt} where phone = #{phone}")
    int updatePassByPhone1(User user);






}
