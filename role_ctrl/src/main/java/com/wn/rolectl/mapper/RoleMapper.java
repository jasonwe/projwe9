package com.wn.rolectl.mapper;

import com.wn.rolectl.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @program: projwe9
 * @description: 角色
 * @author: Jason-Wei
 * @create: 2021-01-27 11:47
 **/
public interface RoleMapper {

    @Select("select * from m_role")
    List<Role> getAllRole();


    @Select("select name from m_role where id = #{id}")
    String getRoleName(@Param("id") int id);
}
