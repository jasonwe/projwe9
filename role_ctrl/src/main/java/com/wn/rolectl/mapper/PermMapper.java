package com.wn.rolectl.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * @program: projwe9
 * @description: 路径权限
 * @author: Jason-Wei
 * @create: 2021-01-27 11:57
 **/
public interface PermMapper {

    @Select("select url from m_perm")
    public List<String> getAllUrl();

    @Select("<script>"
            + "SELECT url FROM m_perm WHERE id IN "
            + "<foreach item='item' index='index' collection='pids' open='(' separator=',' close=')'>"
            + "#{item}"
            + "</foreach>"
            + "</script>")
    public Set<String> getAllUrlById(@Param("pids")List<Integer> pids);
}
