package com.wn.rolectl.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @program: projwe9
 * @description: roleperm
 * @author: Jason-Wei
 * @create: 2021-01-27 11:49
 **/
public interface RolePermMapper {

    @Select("select pid from m_role_perm where rid = #{rid}")
    List<Integer> getAllPidByRid(@Param("rid") int rid);
}
