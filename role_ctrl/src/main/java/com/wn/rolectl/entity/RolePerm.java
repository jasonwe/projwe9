package com.wn.rolectl.entity;

import lombok.Data;

/**
 * @program: projwe9
 * @description: 角色api权限表
 * @author: Jason-Wei
 * @create: 2021-01-27 11:45
 **/
@Data
public class RolePerm {

    private int id;
    private int rid;
    private int pid;
}
