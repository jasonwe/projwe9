package com.wn.rolectl.entity;

import lombok.Data;

/**
 * @program: projwe9
 * @description: permission
 * @author: Jason-Wei
 * @create: 2021-01-27 11:41
 **/
@Data
public class Perm {

    private int id;
    private String name;
    private String desc;
    private String url;
}
