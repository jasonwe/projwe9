package com.wn.rolectl.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author Jason-we
 * @program: modules
 * @Description: 用户类
 * @date 2020-12-22-10-46
 **/
@Data
public class User {

    private String nickname;
    private String password;
    private String phone;
    private String mail;
    /* "男" "女" "保密"*/
    private String sex;

    private int id;
    private int role_id;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private boolean rememberMe;


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String salt;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String code;
}
