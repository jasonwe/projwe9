package com.wn.rolectl.entity;

import lombok.Data;

/**
 * @program: projwe9
 * @description: 角色类
 * @author: Jason-Wei
 * @create: 2021-01-07 10:55
 **/
@Data
public class Role {

    private int id;
    private String name;
    private String name_zh;
}
