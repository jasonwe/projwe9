package com.wn.rolectl.entity;

import lombok.Data;

/**
 * @program: projwe9
 * @description: 前端菜单映射
 * @author: Jason-Wei
 * @create: 2021-01-15 09:13
 **/
@Data
public class Menu {

    private int id;
    private String name;
    private String name_zh;
    private String path;
    private String icon;
    private String component;

}
