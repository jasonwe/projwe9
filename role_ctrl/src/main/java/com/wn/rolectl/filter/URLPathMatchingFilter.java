package com.wn.rolectl.filter;

import com.wn.common.utils.SpringContextUtils;
import com.wn.rolectl.entity.User;
import com.wn.rolectl.service.PermService;
import com.wn.rolectl.service.RolePermService;
import com.wn.rolectl.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

/**
 * @program: projwe9
 * @description: 路径权限拦截
 * @author: Jason-Wei
 * @create: 2021-01-15 15:41
 **/

@Slf4j
public class URLPathMatchingFilter extends PathMatchingFilter {

    @Autowired
    PermService permService;

    @Autowired
    UserService userService;

    @Autowired
    RolePermService rolePermService;

    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        if (HttpMethod.OPTIONS.toString().equals((httpServletRequest).getMethod())) {
            httpServletResponse.setStatus(HttpStatus.NO_CONTENT.value());
            return true;
        }

        if (null == permService) {
            permService = SpringContextUtils.getApplicationContext().getBean(PermService.class);
        }
        if (null == userService) {
            userService = SpringContextUtils.getApplicationContext().getBean(UserService.class);
        }
        if (null == rolePermService) {
            rolePermService = SpringContextUtils.getApplicationContext().getBean(RolePermService.class);
        }


        String requestAPI = getPathWithinApplication(request);
        log.warn("requestApi is {}",requestAPI);
        Subject subject = SecurityUtils.getSubject();

        if (!subject.isAuthenticated()) {
            log.warn("未登录用户尝试访问需要登录的接口");
            return false;
        }

        // 判断访问接口是否需要过滤（数据库中是否有对应信息）
        boolean needFilter = permService.needFilter(requestAPI);
        if (!needFilter) {
            return true;
        } else {
            // 判断当前用户是否有相应权限
            boolean hasPermission = false;
            String username = subject.getPrincipal().toString();
            User user = userService.findByUserType(username);
            List<Integer> pids = rolePermService.getAllPidByRid(user.getRole_id());
            Set<String> perms = permService.getAllUrlById(pids);

            for (String api : perms) {
                // 匹配前缀
                if (requestAPI.startsWith(api)) {
                    hasPermission = true;
                    break;
                }
            }

            if (hasPermission) {
                log.trace("用户：" + username + "访问了：" + requestAPI + "接口");
                return true;
            } else {
                log.warn( "用户：" + username + "访问了没有权限的接口：" + requestAPI);
                return false;
            }
        }
    }
}
