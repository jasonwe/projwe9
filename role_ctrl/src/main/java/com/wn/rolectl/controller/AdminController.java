package com.wn.rolectl.controller;

import com.wn.common.result.Code;
import com.wn.common.result.Result;
import com.wn.common.result.ResultHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: proj_home
 * @description: 管理员类
 * @author: JasonWe
 * @create: 2021-01-26 21:42
 **/
@RestController
@RequestMapping("/module/admin")
@Slf4j
public class AdminController {

    @Autowired
    private ResultHandler resultHandler;

    @GetMapping(value = "/test" , produces = "application/json;charset=utf-8")
    public Result testAdmin(){
        return resultHandler.handleResult(Code.SUCCESS,"ok");
    }
}
