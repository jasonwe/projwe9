package com.wn.rolectl.controller;

import cn.hutool.json.JSONObject;
import com.wn.common.result.Code;
import com.wn.common.result.Result;
import com.wn.common.result.ResultHandler;
import com.wn.rolectl.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: modules
 * @description: mail send
 * @author: JasonWe
 * @create: 2020-12-27 18:59
 **/
@RestController
@RequestMapping("/module")
@Slf4j
public class MailController {

    @Autowired
    private MailService mailService;

    @Autowired
    private ResultHandler resultHandler;

    @GetMapping(value = "/mailCode" ,produces = "application/json;charset=utf-8")
    public Result getMailCode(@RequestParam("mailNo")String mailNo, HttpServletRequest request){
        String code = mailService.sendVertifyCode(mailNo);
        JSONObject object = new JSONObject();
        object.set("mailCode",code);
        object.set("ctime",System.currentTimeMillis());
        object.set("mailNo",mailNo);
        request.getSession().setAttribute("mailCodeVerify"+mailNo,object);
        log.warn("session :{}", request.getSession().getAttribute("mailCodeVerify"+mailNo).toString());
        return resultHandler.handleResult(Code.SUCCESS,"验证码已发送",code);
    }
}
