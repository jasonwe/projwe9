package com.wn.rolectl.controller;

import cn.hutool.json.JSONObject;
import com.wn.common.result.Code;
import com.wn.common.result.Result;
import com.wn.common.result.ResultHandler;
import com.wn.rolectl.entity.User;
import com.wn.rolectl.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @program: projwe9
 * @description: 登录
 * @author: Jason-Wei
 * @create: 2021-01-15 10:51
 **/
@Slf4j
@RestController
@RequestMapping("/module")
public class LoginController {


    @Autowired
    private UserService userService;

    @Autowired
    private ResultHandler resultHandler;

    @PostMapping(value = "/login" ,produces = "application/json;charset=utf-8")
    public Result loginByPassword(@RequestBody User user){
        String loginStr = user.getPhone()==null ? user.getMail() : user.getPhone();
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(loginStr,user.getPassword());
        usernamePasswordToken.setRememberMe(user.isRememberMe());
        try {
            subject.login(usernamePasswordToken);

            return resultHandler.handleResult(Code.SUCCESS,loginStr);
        } catch (IncorrectCredentialsException e) {
            return resultHandler.handleResult(Code.FAIL,"密码错误",null);
        } catch (UnknownAccountException e) {
            return resultHandler.handleResult(Code.FAIL,"账号不存在",null);
        }
    }

    @PostMapping(value = "/register" ,produces = "application/json;charset=utf-8")
    public Result regisUser(@RequestBody User user, HttpServletRequest request){
        User u;
        JSONObject object;
        if(user.getPhone() == null){
            u = userService.getUserByMail(user.getMail());
            object = (JSONObject) request.getSession().getAttribute("mailCodeVerify"+user.getMail());
        }else{
            u = userService.getUserByPhone(user.getPhone());
            object = (JSONObject) request.getSession().getAttribute("mailCodeVerify"+user.getPhone());
        }
        if(Objects.nonNull(u)){
            return resultHandler.handleResult(Code.FAIL,"此号已被注册",null);
        }else if(Objects.isNull(object)){
            return resultHandler.handleResult(Code.FAIL,"验证码已过期",null);
        }else if(!user.getCode().equals(object.getStr("mailCode"))){
            return resultHandler.handleResult(Code.FAIL,"验证码错误",null);
        }else{
            if(userService.userRegis(user) == 1){
                return resultHandler.handleResult(Code.SUCCESS,"注册成功",null);
            }else{
                return resultHandler.handleResult(Code.FAIL,"注册用户失败",user);
            }
        }
    }

    @PostMapping(value = "/resetPass" , produces = "application/json;charset=utf-8")
    public Result resetPass(@RequestBody User user){
        int ret = userService.userPassReset(user);
        if(ret == 1 ){
            return resultHandler.handleResult(Code.SUCCESS,"修改成功",null);
        }else {
            return resultHandler.handleResult(Code.FAIL,"修改失败",user);
        }
    }

}
