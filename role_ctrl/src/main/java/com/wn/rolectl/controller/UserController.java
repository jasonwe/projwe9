package com.wn.rolectl.controller;

import cn.hutool.json.JSONObject;
import com.wn.common.result.Code;
import com.wn.common.result.Result;
import com.wn.common.result.ResultHandler;
import com.wn.rolectl.entity.User;
import com.wn.rolectl.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @author Jason-we
 * @program: modules
 * @Description: 用户控制器类
 * @date 2020-12-22-11-00
 **/
@RestController
@RequestMapping("/module/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ResultHandler resultHandler;

    @Autowired
    private StringEncryptor stringEncryptor;

    @GetMapping(value = "/getAllUser" , produces = "application/json;charset=utf-8")
    public Result getAllUser(){
        List<User> users = userService.getAllUser();
        if(users.isEmpty()){
            log.info("users: {} " , users.toString() );
            return resultHandler.handleResult(Code.FAIL,users);
        }else {
            log.info("users: {} " , users.toString() );
            return resultHandler.handleResult(Code.SUCCESS,users);
        }
    }

/*    @PostMapping(value = "/register" ,produces = "application/json;charset=utf-8")
    public Result regisUser(@RequestBody User user){
        User u;
        if(user.getPhone() == null){
            u = userService.getUserByMail(user.getMail());
        }else{
            u = userService.getUserByPhone(user.getPhone());
        }
        if(Objects.nonNull(u)){
            return resultHandler.handleResult(Code.FAIL,"此号已被注册",user);
        }else{
            String passEncode = stringEncryptor.encrypt(user.getPassword());
            user.setPassword(passEncode);
            if(userService.addUser(user) == 1){
                return resultHandler.handleResult(Code.SUCCESS,"注册成功",null);
            }else{
                return resultHandler.handleResult(Code.FAIL,"注册用户失败",user);
            }
        }
    }*/

    @GetMapping(value = "/regis" ,produces = "application/json;charset=utf-8")
    public Result regis(@RequestParam("regType")String regType, @RequestParam("regNum")String regNum,@RequestParam("code")String code,
                            @RequestParam("nickname")String nickname, @RequestParam("password")String password, HttpServletRequest request){
        User u;
        JSONObject object = (JSONObject) request.getSession().getAttribute("mailCodeVerify"+regNum);
        if(regType.equals("mail")){
            u = userService.getUserByMail(regNum);
        }else{
            u = userService.getUserByPhone(regNum);
        }
        if(Objects.nonNull(u)){
            return resultHandler.handleResult(Code.FAIL,"此号已被注册",regNum);
        }else if(Objects.isNull(object)){
            return resultHandler.handleResult(Code.FAIL,"验证码已过期",code);
        }else if(!code.equals(object.getStr("mailCode"))){
            return resultHandler.handleResult(Code.FAIL,"验证码错误",code);
        }else{
            String passEncode = stringEncryptor.encrypt(password);
            User user = new User();
            user.setNickname(nickname);
            if(regType.equals("mail")){
                user.setMail(regNum);
            }else{
                user.setPhone(regNum);
            }
            user.setPassword(passEncode);
            if(userService.addUser(user) == 1){
                return resultHandler.handleResult(Code.SUCCESS,"注册成功",null);
            }else{
                return resultHandler.handleResult(Code.FAIL,"注册用户失败",user);
            }
        }
    }


/*    @PostMapping(value = "/login" ,produces = "application/json;charset=utf-8")
    public Result loginByPassword(@RequestBody User user){
        String pwdEncoded = "";
        if(user.getPhone() == null){
            pwdEncoded = userService.getPassByMail(user.getMail());
        }else{
            pwdEncoded = userService.getPassByPhone(user.getPhone());
        }
        if(Objects.isNull(pwdEncoded) || pwdEncoded.isEmpty()){
            return resultHandler.handleResult(Code.FAIL,"账号不存在",user);
        }else{
            String pwdDecoded = stringEncryptor.decrypt(pwdEncoded);
            if(pwdDecoded.equals(user.getPassword())){
                return resultHandler.handleResult(Code.SUCCESS,null);
            }else{
                return resultHandler.handleResult(Code.FAIL,"密码错误",user);
            }
        }
    }*/

/*    @GetMapping(value = "/resetPass" , produces = "application/json;charset=utf-8")
    public Result resetPass(@RequestParam("pheormal")String pheormal,@RequestParam("newPass")String newPass){
        int ret = -1;
        log.warn("pheormal:{} , newPass : {} ",pheormal,newPass);
        String pwdEncode  = stringEncryptor.encrypt(newPass);
        if(pheormal.indexOf('@') != -1){
            ret = userService.updatePassByMail(pwdEncode,pheormal);
        }else {
            ret = userService.updatePassByPhone(pwdEncode,pheormal);
        }
        log.warn("ret : {}",ret);
        if(ret == 1 ){
            return resultHandler.handleResult(Code.SUCCESS,"修改成功",null);
        }else {
            return resultHandler.handleResult(Code.FAIL,"修改失败",newPass);
        }
    }*/


    @GetMapping("/logout")
    public Result logOut(){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return resultHandler.handleResult(Code.SUCCESS,"登出成功");
    }

    @GetMapping(value = "/getRole" , produces = "application/json;charset=utf-8")
    public Result getUserRole(@RequestParam("user")String user){
        String roleName = userService.getRole(user);
        return resultHandler.handleResult(Code.SUCCESS,roleName);
    }
}
