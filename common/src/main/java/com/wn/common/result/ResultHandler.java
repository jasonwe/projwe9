package com.wn.common.result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: projwe9
 * @description: 处理返回对象
 * @author: Jason-Wei
 * @create: 2021-01-06 17:10
 **/
@Service
public class ResultHandler {

    @Autowired
    Result result;

    public Result handleResult(Code respCode,String msg,Object data){
        result.setCode(respCode.getCode());
        result.setMsg(msg);
        result.setData(data);
        return result;
    }

    public Result handleResult(Code respCode,Object data){
        result.setData(data);
        result.setCode(respCode.getCode());
        result.setMsg(respCode.getDesc());
        return result;
    }
}
