package com.wn.common.result;

/**
 * @program: projwe9
 * @description: 返回码
 * @author: Jason-Wei
 * @create: 2021-01-06 17:13
 **/
@SuppressWarnings("ALL")
public enum Code {

    SUCCESS(200,"成功"),
    FAIL(101,"失败");

    private int code ;
    private String desc;

    Code(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
