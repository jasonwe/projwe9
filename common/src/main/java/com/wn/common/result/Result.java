package com.wn.common.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @program: projwe9
 * @description: 返回值
 * @author: Jason-Wei
 * @create: 2021-01-06 17:10
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Result {

    private int code;
    private String msg;
    private Object data;
}
