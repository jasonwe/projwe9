package com.wn.common.aop;

import com.wn.common.result.Result;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @program: projwe9
 * @description: 自定义异常处理类
 * @author: Jason-Wei
 * @create: 2021-01-07 16:38
 **/
@Data
@AllArgsConstructor
public class MyException extends Exception{

    //exception 对象
    private Exception exception;

     /*封装返回结果的对象*/
    private Result result;

}
